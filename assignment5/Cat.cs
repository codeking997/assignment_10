﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public abstract class Cat : Animal 
    {
        public string catRace { get; set; }
        public override void MakeNoise()
        {
            Console.WriteLine("Mjau mjau");
        }
        public  void ChaseMice()
        {
            Console.WriteLine("Get the mice quickly!!!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public class BorderCollie : Dog
    {
        public BorderCollie()
        {
            dogRace = nameof(BorderCollie);
        }
        public virtual void ChaseSheep()
        {
            Console.WriteLine("100% energetic all the time and chases sheep");
        }
        public  virtual  void OpenDoor()
        {
            Console.WriteLine("knows how to open the door");
        }
        public  virtual  void SpeaksPortugeese()
        {
            Console.WriteLine("Panjero? Panjero?");
        }
    }
}

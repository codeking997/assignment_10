﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public abstract class Dog : Animal
    {
        public string dogRace { get; set; }
        public override void MakeNoise()
        {
            Console.WriteLine("wofwofwof");
        }
        public virtual void MarkTerritory()
        {
            Console.WriteLine("Pie on the corner");
        }
    }
}

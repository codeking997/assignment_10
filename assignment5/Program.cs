﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace assignment5
{
    class Program
    {
        static BorderCollie borderCollie = new BorderCollie();

        static Puddle puddle = new Puddle();

        static Lion lion = new Lion();

        static Tiger tiger = new Tiger();

        private static List<Dog> dogList = new List<Dog>();

        private static List<Cat> catList = new List<Cat>();

        /*
         *I call the dog method and the cat method in order to populate their collection
         *I call instructions in Main as well which holds the whole program together.
         * 
         */
        static void Main(string[] args)
        {


            //HamsterMethod();

            DogMethod();

            

            CatMethod();

            //Search();
            //SearchName();
            Instructions();

            
        }/*
          * this method gives instructions to users
          * allows users to chose between dog or cat
          * then it allows the user to chose what they want to search for like age or name
          */

        private static void Instructions()
        {
            Console.WriteLine("Welcome to Carl's Zoo if you would like to search for dogs type dog, if you would like to search for" +
                "cats type cats");
            string userInput = Console.ReadLine();
            if (userInput == "cat")
            {
                Console.WriteLine("Welcome to Carl's Zoo if you would like to search for names write name, if you want to search by age older than the age you typed, type age older" +
                    ", if you want to find cat younger than the age you provide type age younger");
                string userInputCat = Console.ReadLine();

                if (userInputCat == "name")
                {
                    SearchName();
                }
                else if (userInputCat == "age older")
                {
                    SearchAgeOlder();
                }
                else if (userInputCat == "age younger")
                {
                    SearchAgeYounger();
                }
            }else if (userInput == "dog")
            {
                Console.WriteLine("Welcome to Carl's Zoo if you would like to search for names write name, if you want to search by age older than the age you typed, type age older" +
                   ", if you want to find dog younger than the age you provide type age younger");
                string userInputDog = Console.ReadLine();
                if (userInputDog == "name")
                {
                    DogSearchName();
                }else if(userInputDog=="age older")
                {
                    DogSearchOldAge();
                }else if(userInputDog=="age younger")
                {
                    SearchDogYounger();
                }
            }
        }
        /*
         * this method searches through the cat names. 
         * the user input is the input from the user which is checked towards the collection 
         * a string comparision is used in order to improve the search by making it case insensitive
         * the string comparison allows the user to find all the cats by just typing a matching letter
         * the while loop is there so that one can repeat the search as many times as one wants to
         * the foreach loop makes it possible to print to the console all elements in the cat list
         */
        private static void SearchName()
        {
            while (true)
            {
                Console.WriteLine("Search for the names of the cats");
                string userInput = Console.ReadLine();
                Console.WriteLine("you wrote: " + userInput);

                var catInfo = from cat in catList
                              where cat.Name.Contains(userInput, StringComparison.CurrentCultureIgnoreCase) //CatAge
                              select cat;
                foreach (var item in catInfo)
                {
                    Console.WriteLine("name of cat: " + item.Name);
                }
            }
        }
        /*
         * this method searches through the younger cats, this will retrive all cats younger than the user provided input
         * uses a while loop to make sure the program can run as many times as the user wishes
         * the foreach loop makes it possible to print to the console all elements in the cat list
         */
        private static void SearchAgeYounger()
        {
            while (true)
            {
                Console.WriteLine("Search for the age of the cat");
                string userInput = Console.ReadLine();
                int yes = Convert.ToInt32(userInput);



                var catInfo = from cat in catList
                              where cat.age < yes
                              select cat;
                foreach (var item in catInfo)
                {
                    Console.WriteLine("age of cat: " + item.age + " " + "name of cat: " + item.Name);
                }
            }
        }
        /*
         * this method makes it possible to search for cats older than the user input.
         * uses a while loop to make sure the program can run as many times as the user wishes
         * the foreach loop makes it possible to print to the console all elements in the cat list
         */
        private static  void SearchAgeOlder()
        {
            while(true){
                Console.WriteLine("Search for the age of the cat");
                string userInput = Console.ReadLine();
                int yes = Convert.ToInt32(userInput);



                var catInfo = from cat in catList
                              where cat.age > yes 
                              select cat;
                foreach (var item in catInfo)
                {
                    Console.WriteLine("age of cat: " + item.age + " " + "name of cat: " + item.Name);
                }
            }


        }
        /*this method searches through the dog names.
         * the user input is the input from the user which is checked towards the collection
         * a string comparision is used in order to improve the search by making it case insensitive
         * the string comparison allows the user to find all the cats by just typing a matching letter
         * the while loop is there so that one can repeat the search as many times as one wants to
         * the foreach loop makes it possible to print to the console all elements in the dog list
         */
        private static void DogSearchName() {
            while (true)
            {
                Console.WriteLine("Search for the names of the dogs");
                string userInput = Console.ReadLine();
                Console.WriteLine("you wrote: " + userInput);

                var dogInfo = from dog in dogList
                              where dog.Name.Contains(userInput, StringComparison.CurrentCultureIgnoreCase) 
                              select dog;
                foreach (var item in dogInfo)
                {
                    Console.WriteLine("name of dog: " + item.Name);
                }
            }

        }
        //works the same as the cat old age
        private static void DogSearchOldAge()
        {
            while (true)
            {
                Console.WriteLine("Search for the age of the dog");
                string userInput = Console.ReadLine();
                int yes = Convert.ToInt32(userInput);



                var dogInfo = from dog in dogList
                              where dog.age > yes
                              select dog;
                foreach (var item in dogInfo)
                {
                    Console.WriteLine("age of dog: " + item.age + " " + "name of dog: " + item.Name);
                }
            }
        }
        //works the same as the cat younger method
        private static void SearchDogYounger()
        {
            while (true)
            {
                Console.WriteLine("Search for the age of the dog");
                string userInput = Console.ReadLine();
                int yes = Convert.ToInt32(userInput);



                var dogInfo = from dog in dogList
                              where dog.age < yes
                              select dog;
                foreach (var item in dogInfo)
                {
                    Console.WriteLine("age of dog: " + item.age + " " + "name of dog: " + item.Name);
                }
            }
        }
        //adds cats to the catlist
        private static  void CatMethod()
        {
            //List<Cat> catList = new List<Cat>();
            catList.Add(new Tiger() { Name = "Mittens", age = 2 });
            catList.Add(new Lion() { Name = "Findus", age = 4 });
            catList.Add(new Tiger() { Name = "James", age = 1 });
            catList.Add(new Lion() { Name = "Mark", age = 7 });
            catList.Add(new Tiger() { Name = "Roger", age = 10 });
            catList.Add(new Lion() { Name = "Tom", age = 1 });
            catList.Add(new Tiger() { Name = "Mort", age = 0 });
            
        }
        //adds hamsters to the hamster list
        private static void HamsterMethod()
        {
            List<Hamster> hamList = new List<Hamster>();
            hamList.Add(new Hamster() { Name = "Hamtaro", age = 2 });
            hamList.Add(new Hamster() { Name = "Kiko", age = 3 });
            foreach (Hamster aHamster in hamList)
            {
                Console.WriteLine($"{aHamster.Name}, {aHamster.age}");
                aHamster.Eat();
                aHamster.MakeNoise();
                aHamster.RunInWheel();
            }

        }
        //adds dogs to the dog list
        private static  void DogMethod()
        {
            //List<Dog> dogList = new List<Dog>();
            dogList.Add(new Puddle() { Name = "Fido", age = 5 });
            dogList.Add(new BorderCollie() { Name = "Rex", age = 14 });
            dogList.Add(new Puddle() { Name = "Trond", age = 10 });
            dogList.Add(new BorderCollie() { Name = "Pachino", age = 1 });
            dogList.Add(new Puddle() { Name = "Leo", age = 3 });
            dogList.Add(new BorderCollie() { Name = "Tom", age = 8 });
            dogList.Add(new Puddle() { Name = "Frank", age = 2 });
            dogList.Add(new BorderCollie() { Name = "Paul", age = 18 });

            
        }
    }
}

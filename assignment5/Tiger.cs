﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public class Tiger : Cat, ISwim
    {
        public Tiger()
        {
            catRace = nameof(Tiger);
        }
        public  virtual  void TakesBath()
        {
            Console.WriteLine("Tigers love to take a bath");
        }
        public  void Swim()
        {
            Console.WriteLine("Nice and relaxing going for a swim");
        }
    }
}
